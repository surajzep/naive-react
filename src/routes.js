import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import AboutComponent from "./components/about";
import APICall from "./components/apiCall";
import ContactComponent from "./components/contactState";
import DynamicRoute from "./components/dynamicRoute";
import DigitalClock from "./components/digitalClock";
import HomeComponent from "./components/home";

class Routes extends Component {
  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route path="/" exact>
              <HomeComponent />
            </Route>
            <Route path="/about" exact>
              <AboutComponent />
            </Route>
            <Route path="/contact" exact>
              <ContactComponent />
            </Route>
            <Route path="/clock" exact>
              <DigitalClock />
            </Route>
            <Route path="/apicall" exact>
              <APICall />
            </Route>
            <Route path="/dynamicroute/:id" exact>
              <DynamicRoute />
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Routes;

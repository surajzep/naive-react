import React, { Component } from "react";

class CovidCard extends Component {
  render() {
    let item = this.props.data;
    return (
      <div style={{ marginTop: 40 }}>
        <h3>{item.country}</h3>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginBottom: 0,
            paddingBottom: 0,
          }}
        >
          <h5>Total Cases </h5>
          <h5>Total Deaths</h5>
          <h5>Total Recovered</h5>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: 0,
            paddingTop: 0,
          }}
        >
          <div style={{ color: "#185cc9" }}>{item.cases}</div>
          <div style={{ color: "#c92a18" }}>{item.deaths}</div>
          <div style={{ color: "#21660a" }}>{item.recovered}</div>
        </div>
      </div>
    );
  }
}

export default CovidCard;

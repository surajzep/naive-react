import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class DynamicRoute extends Component {
  render() {
    return (
      <div>
        {console.log(this.props.match.params.id)}
        <p>This is a dynamic route!</p>
      </div>
    );
  }
}

export default withRouter(DynamicRoute);

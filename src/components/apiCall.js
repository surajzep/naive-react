import React, { Component } from "react";
import axios from "axios";
import CovidCard from "./covidCard";
import ErrorComponent from "./error";

const API_URL = "https://coronavirus-19-api.herokuapp.com/countries";

class APICall extends Component {
  constructor(props) {
    super(props);
    this.state = {
      covidData: [],
      displayData: [],
      loading: true,
      error: false,
    };
  }

  getRemoteData() {
    let self = this;
    axios
      .get(API_URL)
      .then(function (response) {
        self.setState({
          covidData: response.data,
          displayData: response.data,
          loading: false,
        });
        // handle success
        console.log(this);
        console.log(response.data.length);
        console.log(response.data[0].country);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
        self.setState({
          error: true,
        });
      })
      .finally(function () {
        console.log("Always Executed!!");
      });
  }

  onRetry = () => {
    this.setState({
      error: false,
      loading: true,
    });
    this.getRemoteData();
  };

  filterCountry = (event) => {
    console.log(event.target.value);
    let searchText = event.target.value.toLowerCase();
    let allItems = this.state.covidData;
    let filteredItems = allItems.filter((item) =>
      item.country.toLowerCase().includes(searchText)
    );
    console.log(filteredItems);
    this.setState({
      displayData: filteredItems,
    });
  };

  componentDidMount() {
    this.getRemoteData();
  }

  render() {
    return (
      <div>
        <h2>COVID-19 Updates</h2>
        {this.state.error ? (
          <ErrorComponent onRetry={this.onRetry} />
        ) : !this.state.loading ? (
          <div>
            <input
              type="text"
              placeholder="Search for a Country"
              onChange={this.filterCountry}
              style={{ width: "40%" }}
            />
            {this.state.displayData.map((element, id) => (
              <CovidCard data={element} key={id} />
            ))}
          </div>
        ) : null}
      </div>
    );
  }
}

export default APICall;

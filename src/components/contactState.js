import React, { Component } from "react";
import PropsComponent from "./contactProps";
import { Link, withRouter } from "react-router-dom";

class ContactComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "Suraj Joshi",
      contactNum: "+977-9868853509",
      emailAddress: "surajjoshi0123@gmail.com",
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    return (
      <div>
        <Link to={"/clock"}>
          <button>Digital Clock!</button>
        </Link>
        <input
          type="text"
          name="fullName"
          placeholder="Full Name"
          onChange={this.handleChange}
        />
        <input
          type="text"
          name="contactNum"
          placeholder="Phone No."
          onChange={this.handleChange}
        />
        <input
          type="email"
          name="emailAddress"
          placeholder="Email Address"
          onChange={this.handleChange}
        />
        <PropsComponent
          fullName={this.state.fullName}
          contactNumber={this.state.contactNum}
          emailAddress={this.state.emailAddress}
        />
        <button
          onClick={() =>
            this.props.history.push("/clock", {
              requestedBy: this.state.fullName,
            })
          }
        >
          Change Route Programmatically!
        </button>
        {console.log(this.props)}
      </div>
    );
  }
}

export default withRouter(ContactComponent);

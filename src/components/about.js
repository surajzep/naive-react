import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class AboutComponent extends Component {
  render() {
    return (
      <div>
        <p>This is about Page made using React</p>
        <button onClick={() => this.props.history.push("/dynamicroute/5")}>
          Route Dynamically
        </button>
      </div>
    );
  }
}

export default withRouter(AboutComponent);

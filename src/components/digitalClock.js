import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class DigitalClock extends Component {
  constructor(props) {
    super(props);
    let displayTime = this.getCurrentTime();
    this.state = {
      timeKey: displayTime,
    };
  }

  getCurrentTime() {
    let timeNow = new Date();
    let hrNow = timeNow.getHours();
    let minNow = timeNow.getMinutes();
    let secNow = timeNow.getSeconds();

    if (hrNow < 10) {
      hrNow = "0" + hrNow;
    }
    if (minNow < 10) {
      minNow = "0" + minNow;
    }

    if (secNow < 10) {
      secNow = "0" + secNow;
    }

    let displayTime = hrNow + " : " + minNow + " : " + secNow;
    return displayTime;
  }

  updateTime() {
    let currentTime = this.getCurrentTime();
    this.setState({
      timeKey: currentTime,
    });
  }

  componentDidMount() {
    console.log(this.props);
    this.timer = setInterval(() => {
      this.updateTime();
    }, 500);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return (
      <div>
        <h4>{this.state.timeKey}</h4>
        <br />
        <br />
        <p>
          Thank You {this.props.location.state.requestedBy} for visting the
          site!
        </p>
      </div>
    );
  }
}

export default withRouter(DigitalClock);

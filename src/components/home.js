import React, { Component } from "react";

class HomeComponent extends Component {
  render() {
    return (
      <div>
        <p>This is the home page of our App</p>
      </div>
    );
  }
}

export default HomeComponent;

import React, { Component } from "react";

class PropsComponent extends Component {
  render() {
    return (
      <div>
        <p>Full Name : {this.props.fullName}</p>
        <p> Contact Number: {this.props.contactNumber}</p>
        <p>Email Address : {this.props.emailAddress}</p>
      </div>
    );
  }
}

export default PropsComponent;

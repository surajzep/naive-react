import React, { Component } from "react";

class ErrorComponent extends Component {
  render() {
    return (
      <div>
        <h3>Something Went Wrong!</h3>
        <button
          onClick={() => {
            this.props.onRetry();
          }}
        >
          Try Again!
        </button>
      </div>
    );
  }
}

export default ErrorComponent;

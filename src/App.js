import React, { Component } from "react";
import "./App.css";

import Routes from "./routes";

class App extends Component {
  render() {
    return (
      <div className="header">
        <p>Hey, We gonna explore React Router World through this app!</p>
        <Routes />
      </div>
    );
  }
}

export default App;
